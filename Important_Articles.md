# Important Articles
* https://towardsdatascience.com/anomaly-detection-with-local-outlier-factor-lof-d91e41df10f2
* https://www.datatechnotes.com/2020/04/anomaly-detection-with-local-outlier-factor-in-python.html
* https://deepnote.com/@bit.io/Local-Outlier-Factor-with-Scikit-Learn-5w3IhpJpSU-6alPjLjULIQ
* https://towardsdatascience.com/is-normal-distribution-necessary-in-regression-how-to-track-and-fix-it-494105bc50dd
* https://www.appier.com/blog/5-types-of-regression-analysis-and-when-to-use-them/
* https://stats.stackexchange.com/questions/75054/how-do-i-perform-a-regression-on-non-normal-data-which-remain-non-normal-when-tr
* https://www.researchgate.net/post/Can_we_do_regression_analysis_with_non_normal_data_distribution
* https://machinelearningmastery.com/quantile-transforms-for-machine-learning/
* https://towardsdatascience.com/5-feature-selection-method-from-scikit-learn-you-should-know-ed4d116e4172
* https://machinelearningmastery.com/feature-selection-for-regression-data/
* https://www.statology.org/f-test-python/
